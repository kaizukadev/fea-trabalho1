<?php
class Tarefas extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("tarefas_model");
        $this->load->helper("url_helper");
        $this->load->library("sistema");
    }

    public function editar()
    {
        $id = $this->uri->segment(3);
        $dados = array();

        if (empty($id)) {
            show_404();
        } else {
            $this->load->helper('form');
            $dados['tarefa'] = $this->tarefas_model->get_tarefa_by_id($id);
            $dados['pagina'] = 'tarefas/edit';
            $dados['modulo'] = '';
            $dados['kill'] = 0;
            $dados['b_novo'] = 0;
            $dados['modulo'] = 'Tarefas [ EDITAR ]';
            $dados['msgBtn'] = 'Gravar';
            $dados['linkMain'] = '/main';
            $this->load->view('pages', $dados);
        }
    }

    public function incluir()
    {
        $dados = array();
        $this->load->helper('form');
        $dados['tarefa'] = $this->tarefa_void();
        $dados['pagina'] = 'tarefas/edit';
        $dados['modulo'] = '';
        $dados['kill'] = 0;
        $dados['b_novo'] = 0;
        $dados['modulo'] = 'Tarefas [ INCLUIR ]';
        $dados['msgBtn'] = 'Incluir';
        $dados['linkMain'] = '/main';
        $this->load->view('pages', $dados);
    }

    public function listar()
    {
        $dados['tarefas'] = $this->tarefas_model->listartarefas();
        $dados['pagina'] = '/tarefas/browse';
        $dados['kill'] = 2;
        $dados['modulo'] = 'CADASTRO DE TAREFAS';
        $dados['b_novo'] = 1;
        $dados['b_action'] = 'tarefas/incluir';
        $dados['linkMain'] = '/main';
        $this->load->view('pages', $dados);
    }

    public function store()
    {
        $id = $this->input->post('f_id');
        $tarefa = array(
            'descricao' => $this->input->post('f_descricao'),
            'tempo_p' => $this->input->post('f_tempo_p'),
        );

        if ($id != 0) {
            $ret = $this->tarefas_model->gravar($id, $tarefa);
            if ($ret == 1) $this->sistema->okMsg01('*** Registro ' . $id . ' gravado com sucesso');
            if ($ret != 1) $this->sistema->errorMsg01('*** ERRO! Não foi possível gravar o registro!');
        } else {
            $ret = $this->tarefas_model->inserir($tarefa);
        }
        redirect(base_url('/tarefas/listar'));
    }


    public function tarefa_void()
    {
        $query = $this->db->query("select * from tarefas limit 1");
        $tarefa = $query->row();
        $tarefa->id = '';
        $tarefa->descricao = '';
        $tarefa->tempo_p = 0;
        return $tarefa;
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);
        $snf = -1;
        if (empty($id)) {
            show_404();
        } else {
            $idex = $id;
            $snf = $this->tarefas_model->excluir($idex);
            if ($snf == 0) $this->sistema->errorMsg01('*** ERRO! ID=' . $idex . '. Não foi possível EXCLUIR o registro!');
            if ($snf == 1) $this->sistema->okMsg01('*** Registro ID=' . $idex . ' EXCLUÍDO com sucesso');
        }

        //        $this->sistema->mostra();
        //        echo $ret;

        redirect(base_url('tarefas/listar'));

        //               $this->listar();
    }
}
