<?php
class Usuarios extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("usuarios_model");
        $this->load->helper("url_helper");
        $this->load->library("sistema");
    }

    public function editar()
    {
        $id = $this->uri->segment(3);
        $dados = array();

        if (empty($id)) {
            show_404();
        } else {
            $this->load->helper('form');
            $dados['usuario'] = $this->usuarios_model->get_usuario_by_id($id);
            $dados['pagina'] = 'usuarios/edit';
            $dados['modulo'] = '';
            $dados['kill'] = 0;
            $dados['b_novo'] = 0;
            $dados['modulo'] = 'Usuários [ EDITAR ]';
            $dados['msgBtn'] = 'Gravar';
            $dados['linkMain'] = '/main';
            $this->load->view('pages', $dados);
        }
    }

    public function incluir()
    {
        $dados = array();
        $this->load->helper('form');
        $dados['usuario'] = $this->usuario_void();
        $dados['pagina'] = 'usuarios/edit';
        $dados['modulo'] = '';
        $dados['kill'] = 0;
        $dados['b_novo'] = 0;
        $dados['modulo'] = 'Usuários [ INCLUIR ]';
        $dados['msgBtn'] = 'Incluir';
        $dados['linkMain'] = '/main';
        $this->load->view('pages', $dados);
    }

    public function listar()
    {
        $dados['usuarios'] = $this->usuarios_model->listarUsuarios();
        $dados['pagina'] = '/usuarios/browse';
        $dados['kill'] = 3;
        $dados['modulo'] = 'CADASTRO DE USUÁRIOS';
        $dados['b_novo'] = 1;
        $dados['b_action'] = 'usuarios/incluir';
        $dados['linkMain'] = '/main';
        $this->load->view('pages', $dados);
    }

    public function store()
    {
        $id = $this->input->post('f_id');
        $usuario = array(
            'username' => $this->input->post('f_username'),
            'userpwd' => $this->input->post('f_userpwd'),
            'nome' => $this->input->post('f_nome'),
            'cpf' => $this->input->post('f_cpf'),
            'cargo' => $this->input->post('f_cargo'),
            'email' => $this->input->post('f_email'),
            'adm_user' => $this->input->post('f_adm') + 0
        );

        if ($id != 0) {
            $ret = $this->usuarios_model->gravar($id, $usuario);
            if ($ret == 1) $this->sistema->okMsg01('*** Registro ' . $id . ' gravado com sucesso');
            if ($ret != 1) $this->sistema->errorMsg01('*** ERRO! Não foi possível gravar o registro!');
        } else {
            $ret = $this->usuarios_model->inserir($usuario);
        }
        redirect(base_url('/usuarios/listar'));
        //        $this->listar();
    }


    public function usuario_void()
    {
        $query = $this->db->query("select * from usuarios limit 1");
        $usuario = $query->row();
        $usuario->id = '';
        $usuario->username = '';
        $usuario->userpwd = '';
        $usuario->nome = '';
        $usuario->cpf = '';
        $usuario->cargo = '';
        $usuario->email = '';
        $usuario->adm_user = 0;
        return $usuario;
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);
        #snf=-1;
        if (empty($id)) {
            show_404();
        } else {
            $idex = $id;
            $snf = $this->usuarios_model->excluir($idex);
            if ($snf == 0) $this->sistema->errorMsg01('*** ERRO! ID=' . $idex . '. Não foi possível EXCLUIR o registro!');
            if ($snf == 1) $this->sistema->okMsg01('*** Registro ID=' . $idex . ' EXCLUÍDO com sucesso');
        }

        //        $this->sistema->mostra();
        //        echo $ret;

        redirect(base_url('usuarios/listar'));

        //               $this->listar();
    }
}
