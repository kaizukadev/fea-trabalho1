<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function index()
    {
        $this->load->helper("url_helper");
        $this->load->helper('form');
        $dados['pagina'] = 'pages/login';
        $dados['modulo'] = '';
        $dados['kill'] = 0;
        $dados['b_novo'] = 0;
        $dados['linkMain'] = '#';
        $this->load->view('pages', $dados);
}

	
public function logar(){
    $usr = $this->input->post("f_usr");
    $pwd = $this->input->post("f_pwd");

    if ($usr == 'admin' && $pwd == '12345' ) {
        $this->session->set_userdata("logado", 1);
        redirect('/main');
    } else {
        $dados['erro'] = "Usuário/Senha Incorretos";
        $this->load->helper("url_helper");
        $this->load->helper('form');
        $dados['pagina'] = 'pages/login';
        $dados['modulo'] = '';
        $dados['kill'] = 0;
        $dados['b_novo'] = 0;
        $dados['linkMain'] = '#';
        $this->load->view('pages', $dados);
    }
}

/*
 * Aqui eu destruo a variável logado na sessão e redireciono para a url base. Como esta variável não existe mais, o usuário
 * será direcionado novamente para a tela de login.
 */
public function deslogar(){
    $this->session->unset_userdata("logado");
    redirect(base_url());
    
}









}