<?php
class Projetos extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("projetos_model");
        $this->load->helper("url_helper");
        $this->load->library("sistema");
    }

    public function editar()
    {
        $id = $this->uri->segment(3);
        $dados = array();

        if (empty($id)) {
            show_404();
        } else {
            $this->load->helper('form');
            $dados['Projetos'] = $this->projetos_model->get_projeto_by_id($id);
            $dados['pagina'] = 'projetos/edit';
            $dados['modulo'] = '';
            $dados['kill'] = 0;
            $dados['b_novo'] = 0;
            $dados['modulo'] = 'Projetos [ EDITAR ]';
            $dados['msgBtn'] = 'Gravar';
            $dados['linkMain'] = '/main';
            $this->load->view('pages', $dados);
        }
    }

    public function incluir()
    {
        $dados = array();
        $this->load->helper('form');
        $dados['Projetos'] = $this->Projetos_void();
        $dados['pagina'] = 'projetos/edit';
        $dados['modulo'] = '';
        $dados['kill'] = 0;
        $dados['b_novo'] = 0;
        $dados['modulo'] = 'Projetos [ INCLUIR ]';
        $dados['msgBtn'] = 'Incluir';
        $dados['linkMain'] = '/main';
        $this->load->view('pages', $dados);
    }

    public function listar()
    {
        $dados['projetos'] = $this->projetos_model->listarprojetos();
        $dados['pagina'] = '/projetos/browse';
        $dados['kill'] = 1;
        $dados['modulo'] = 'CADASTRO DE PROJETOS';
        $dados['b_novo'] = 1;
        $dados['b_action'] = 'projetos/incluir';
        $dados['linkMain'] = '/main';
        $this->load->view('pages', $dados);
    }

    public function store()
    {
        $id = $this->input->post('f_id');
        $Projetos = array(
            'username' => $this->input->post('f_username'),
            'userpwd' => $this->input->post('f_userpwd'),
            'nome' => $this->input->post('f_nome'),
            'cpf' => $this->input->post('f_cpf'),
            'cargo' => $this->input->post('f_cargo'),
            'email' => $this->input->post('f_email'),
            'adm_user' => $this->input->post('f_adm') + 0
        );

        if ($id != 0) {
            $ret = $this->projetos_model->gravar($id, $Projetos);
            if ($ret == 1) $this->sistema->okMsg01('*** Registro ' . $id . ' gravado com sucesso');
            if ($ret != 1) $this->sistema->errorMsg01('*** ERRO! Não foi possível gravar o registro!');
        } else {
            $ret = $this->projetos_model->inserir($Projetos);
        }
        redirect(base_url('/projetos/listar'));
        //        $this->listar();
    }


    public function Projetos_void()
    {
        $query = $this->db->query("select * from projetos limit 1");
        $Projetos = $query->row();
        $Projetos->id = '';
        $Projetos->username = '';
        $Projetos->userpwd = '';
        $Projetos->nome = '';
        $Projetos->cpf = '';
        $Projetos->cargo = '';
        $Projetos->email = '';
        $Projetos->adm_user = 0;
        return $Projetos;
    }

    public function excluir()
    {
        $id = $this->uri->segment(3);
        #snf=-1;
        if (empty($id)) {
            show_404();
        } else {
            $idex = $id;
            $snf = $this->projetos_model->excluir($idex);
            if ($snf == 0) $this->sistema->errorMsg01('*** ERRO! ID=' . $idex . '. Não foi possível EXCLUIR o registro!');
            if ($snf == 1) $this->sistema->okMsg01('*** Registro ID=' . $idex . ' EXCLUÍDO com sucesso');
        }

        //        $this->sistema->mostra();
        //        echo $ret;

        redirect(base_url('projetos/listar'));

        //               $this->listar();
    }
}
