<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller
{
    public function index()
    {
        $dados['pagina'] = 'pages/main';
        $dados['modulo'] = '';
        $dados['kill'] = -1;
        $dados['b_novo'] = 0;
        $dados['linkMain'] = '#';
        $this->load->view('pages', $dados);
    }
}
