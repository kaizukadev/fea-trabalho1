<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sistema
{

  public function errorMsg01($msg1)
  {
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>' . $msg1 . '
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
  }

  public function okMsg01($msg1)
  {
    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>' . $msg1 . '
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
  }

  public function mostra()
  {
    echo '<script type="text/javascript">
          alert("Registro Confirma a exclusão do registro do ID=");
          window.location.href = "/usuarios/listar";
</script>';
  }





} //Final da classe
