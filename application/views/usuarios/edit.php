<style>
    h11 {
        color: red;
    }
</style>

<div class="row justify-content-center">
    <div class="col-sm-11">
        <div class="card">
            <div class="card-header text-right bg-info">
                <h5><?php echo $modulo ?><h5>
            </div>
            <div class="card-body">

                <!-- Início do formulário -->
                <?php echo form_open('/usuarios/store'); ?>

                <div class="row">
                    <div class="col-md-1">
                        <div class="form-group">
                            <strong>Id</strong>
                            <input type="text" name="f_id" class="form-control text-right" value="<?php echo $usuario->id ?>" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong>Username<h11> *</h11></strong>
                            <input type="text" name="f_username" class="form-control" maxlength="20" value="<?php echo $usuario->username ?>" autofocus required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <strong>Nome Completo<h11> *</h11></strong>
                            <input type="text" name="f_nome" class="form-control" maxlength="50" value="<?php echo $usuario->nome ?>" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong>Senha<h11> *</h11></strong>
                            <input type="password" name="f_userpwd" class="form-control" maxlength="20" value="<?php echo $usuario->userpwd ?>" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <strong>C.P.F.<h11> *</h11></strong>
                            <input type="text" name="f_cpf" class="form-control" maxlength="11" value="<?php echo $usuario->cpf ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <strong>Email<h11> *</h11></strong>
                            <input type="email" name="f_email" class="form-control" maxlength="50" value="<?php echo $usuario->email ?>" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong>Cargo<h11> *</h11></strong>
                            <input type="text" name="f_cargo" class="form-control" maxlength="25" value="<?php echo $usuario->cargo ?>" required>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="f_id" value=" <?php echo $usuario->id; ?>">

                <div class="form-check col-md-3">
                    <input type="checkbox" name="f_adm" class="form-check-input" value=1 <?php if ($usuario->adm_user == 1) echo 'checked' ?>>
                    <label class="form-check-label"><strong>Administrador<h11> *</h11></strong></label>
                </div>
            </div>
            <!-- Final do card -->
        </div>
        <div class="card-footer text-muted">
            <div class="form-group text-right">
                <div class="col-sm-offset-2 col-sm-12">
                    <button type="submit" class="btn btn-success">
                        <?php
                        echo $msgBtn . '</button>';
                        ?>
                        <a class="btn btn-secondary btn-size" href="/usuarios/listar" role="button">Cancelar</a>
                </div>
            </div>
            </form>

        </div> <!-- final  -->

    </div>
</div>