<style>
  body {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  }

  h3 {
    color: darkblue;
  }

  table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
    display: block; 
    /*white-space: nowrap;*/
  }

  #t_browse td,
  #t_browse th {
    border: 1px solid #ddd;
    padding: 8px;
  }

  #t_browse tr:nth-child(even) {
    background-color: #f2f2f2;
  }

  #t_browse tr:hover {
    background-color: #ddd;
  }

  #t_browse th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    /*    background-color: #4CAF50;*/
    background-color: cadetblue;
    color: white;
  }

  .btn_link {
    max-width: 20px;
    max-height: 20px;
  }

  .tdiv {
    max-height: 90%;
    overflow-y: scroll;
    overflow-x: auto;
  }
</style>

<?php echo '<div class="container-fluid bg-info text-center"><strong><em>' . $modulo . '</strong></em></div>'; ?>

<div class="tdiv">
  <table id="t_browse">
    <thead>
      <tr>
        <th class="text-center" style="width: 70px">Ação</th>
        <th class="text-right" style="width: 60px">ID</th>
        <th style="width: 300px">Nome</th>
        <th style="width: 200px">Cliente</th>
        <th class="text-center" style="width: 90px">Horas</th>
        <th style="max-width: 250px">Descrição</th>
        <th class="text-center" style="min-width: 170px">Última Edição</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($projetos as $prj) {
        echo '
          <tr>
          <td><div class="text-center">
          <a href="' . base_url("projetos/editar/" . $prj["id"]) . '"><img src="/assets/img/32_editar.png" class="btn_link tooltip-test" title="Editar"></a>
          <a href="javascript:confExc('. $prj["id"] .');"><img src="/assets/img/32_excluir.png" class="btn_link tooltip-test" title="Excluir"></a>
          </div></td>
          <td class="text-right">' . $prj["id"] . '</td>
          <td>' . $prj["nome"] . '</td>
          <td>' . $prj["cliente"] . '</td>
          <td class="text-center">' . number_format($prj["tempo_p"]/60,2,',','.') . '</td>
          <td>' . $prj["descricao"] . '</td>
          <td class="text-center">' . $prj["ts_edit"] . '</td>
          </tr>';
      }
      ?>
    </tbody>
  </table>
</div>

<script type="text/javascript">
  function confExc(reg) {
    if (confirm('Confirma a exclusão do registro do ID=' + reg))
      window.location.href = "excluir/" + reg;  }
</script>