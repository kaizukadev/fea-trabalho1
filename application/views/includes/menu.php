<div>
<nav class="navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="<?php echo $linkMain ?>"><?php echo SNF_SISTEMA ?></a>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <?php if ($kill != 1 and $kill != 0) echo '<li class="nav-item"><a class="nav-link" href="/projetos/listar">Projetos</a></li>';?>
            <?php if ($kill != 2 and $kill != 0) echo '<li class="nav-item"><a class="nav-link" href="/tarefas/listar">Tarefas</a></li>';?>
            <?php if ($kill != 3 and $kill != 0) echo '<li class="nav-item"><a class="nav-link" href="/usuarios/listar">Usuários</a></li>';?>
        </ul>
        <?php if ($b_novo != 0) echo '<a href="' . base_url($b_action) . '" class="btn btn-warning btn-xs"><strong>Novo</strong></a>'; ?>
        <?php if ($b_novo == 0 and $kill !=0) echo '<a href="' . base_url('login/deslogar/') . '"><img src="/assets/img/32_logout2.png" class="btn_link tooltip-test" title="Logout"></a>'; ?>

    </div>
</nav>
</div>

