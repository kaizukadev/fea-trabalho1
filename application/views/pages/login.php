<style>
  #principal {
    height: 100%;
  }

  #img01 {
    /*  width: 300px;
    height: 150px;
    border: 1px solid black;*/
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
  }
</style>
<div style="min-height: 50px;"></div>

<div class="container-fluid justify-content-center align-items-center row">

  <?php echo form_open('login/logar/', array('class' => 'class="text-center border border-light p-5"')); ?>

  <div class="card" style="width: 24rem; border-width: 4px;">
    <img class="card-img-top" src="/assets/img/main01.jpg" alt="SAP Login">
    <div class="card-body">

      <input type="text" name="f_usr" class="form-control mb-4 mt-4" value="" maxlength="20" placeholder="Usuário" autofocus required>

      <input type="password" name="f_pwd" class="form-control mb-4" value="" maxlength="20" placeholder="Senha" required>

      <button class="btn btn-info btn-block mb-4" type="submit"><b>Login</b></button>

      <?php //$erro = 'Usuario/Senha Inválidos'; ?>

      <?php if (isset($erro)) : ?>
        <div class="alert alert-danger my-2 text-center" role="alert" style="margin-top: 10px;"><b><?php echo $erro; ?></b></div>
      <?php endif; ?>


      </form>

    </div>
  </div>


</div>