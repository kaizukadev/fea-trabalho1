<style>
    h11 {
        color: red;
    }
</style>

<div class="row justify-content-center">
    <div class="col-sm-11">
        <div class="card">
            <div class="card-header text-right bg-info">
                <h5><?php echo $modulo ?><h5>
            </div>
            <div class="card-body">

                <!-- Início do formulário -->
                <?php echo form_open('/tarefas/store'); ?>

                <div class="row">
                    <div class="col-md-1">
                        <div class="form-group">
                            <strong>Id</strong>
                            <input type="text" name="f_id" class="form-control text-right" value="<?php echo $tarefa->id ?>" disabled>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <strong>Descrição<h11> *</h11></strong>
                            <input type="text" name="f_descricao" class="form-control" maxlength="50" value="<?php echo $tarefa->descricao ?>" autofocus required>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <strong>Tempo Previsto<h11> *</h11></strong>
                            <input type="number" name="f_tempo_p" class="form-control text-right" value="<?php echo $tarefa->tempo_p; ?>" min=1 required>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="f_id" value=" <?php echo $tarefa->id; ?>">

            </div>
            <!-- Final do card -->
        </div>
        <div class="card-footer text-muted">
            <div class="form-group text-right">
                <div class="col-sm-offset-2 col-sm-12">
                    <button type="submit" class="btn btn-success">
                        <?php
                        echo $msgBtn . '</button>';
                        ?>
                        <a class="btn btn-secondary btn-size" href="/tarefas/listar" role="button">Cancelar</a>
                </div>
            </div>
            </form>

        </div> <!-- final  -->

    </div>
</div>