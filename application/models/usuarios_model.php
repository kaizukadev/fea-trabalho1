<?php
class Usuarios_Model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function listarUsuarios()
    {
        $query = $this->db->get("usuarios");
        return $query->result_array();
    }

    public function get_usuario_by_id($id)
    {
        $query = $this->db->get_where('usuarios', array('id' => $id));
        return $query->row();
    }


    public function inserir($usuario)
    {
        return $this->db->insert('usuarios', $usuario);
    }

    public function gravar($id, $usuario)
    {
        $this->db->where('id', $id);
        return $this->db->update('usuarios', $usuario);
    }

    public function excluir($id)
    {
        $kid= (int) $id;
        $ok = -1;
        $this->db->get_where('proj_tarefa', array('id_executor' => $kid));
        $nar = $this->db->affected_rows();
        if ( $this->db->affected_rows() != 0 ) {
            $ok = 0;
        } else {
            $this->db->delete('usuarios', array('id' => $kid)); 
            $ok = 1;
        }            
        return $ok;

    }


}