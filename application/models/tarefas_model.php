<?php
class tarefas_Model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function listarTarefas()
    {
        $query = $this->db->get("tarefas");
        return $query->result_array();
    }

    public function get_tarefa_by_id($id)
    {
        $query = $this->db->get_where('tarefas', array('id' => $id));
        return $query->row();
    }


    public function inserir($tarefa)
    {
        return $this->db->insert('tarefas', $tarefa);
    }

    public function gravar($id, $tarefa)
    {
        $this->db->where('id', $id);
        return $this->db->update('tarefas', $tarefa);
    }

    public function excluir($id)
    {
        $kid= (int) $id;
        $ok = -1;
        $this->db->get_where('proj_tarefa', array('id_tarefa' => $kid));
        $nar = $this->db->affected_rows();
        if ( $this->db->affected_rows() != 0 ) {
            $ok = 0;
        } else {
            $this->db->delete('tarefas', array('id' => $kid)); 
            $ok = 1;
        }            
        return $ok;

    }


}