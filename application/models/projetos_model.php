<?php
class Projetos_Model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function listarProjetos()
    {
        $query = $this->db->get("projetos");
        return $query->result_array();
    }

    public function get_projeto_by_id($id)
    {
        $query = $this->db->get_where('projetos', array('id' => $id));
        return $query->row();
    }


    public function inserir($projeto)
    {
        return $this->db->insert('projetos', $projeto);
    }

    public function gravar($id, $projeto)
    {
        $this->db->where('id', $id);
        return $this->db->update('projetos', $projeto);
    }

    public function excluir($id)
    {
        $kid= (int) $id;
        $ok = -1;
        $this->db->get_where('proj_tarefa', array('id_executor' => $kid));
        $nar = $this->db->affected_rows();
        if ( $this->db->affected_rows() != 0 ) {
            $ok = 0;
        } else {
            $this->db->delete('projetos', array('id' => $kid)); 
            $ok = 1;
        }            
        return $ok;

    }


}