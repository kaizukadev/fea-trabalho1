# Especificação do Trabalho 1

Este trabalho consiste em estudar e fazer uso de frameworks para desenvolvimento de uma aplicação Web. Essa aplicação é voltada para realizar um módulo administrativo de um sistema com o tema definido pelo aluno. O framework que deve ser utilizados neste trabalho para esta aplicação é um Frameworks Back-end Fullstack.

Instruções Gerais

Essa aplicação deve ser desenvolvida utilizando um Framework Back-end Fullstack. O aluno poderá escolher qual framework pretende trabalhar, mas deverá justificar adequadamente.

O trabalho consiste em desenvolver uma aplicação com funcionalidades administrativas para um sistema escolhido pelo aluno. Por exemplo, suponhamos que o aluno tenha escolhido um sistema de vendas comerciais como escopo, esse módulo do sistema será responsável por cadastros (produtos, categorias, marca, fornecedores), compra de produtos, controle de estoque e cadastro de usuários administrativos (outros funcionários), além do próprio login.

Seguindo essa ideia, sugere-se que sejam definidas as seguintes funcionalidades com persistência de dados:

    Cadastros de Entidades: consiste em um CRUD (Create-Retrieve-Update-Delete) de uma determinada entidade (no exemplo acima seria produtos, categorias, etc). Uma dica importante é relacionar identificadores (“id”) para essas entidades. Pelo menos dois tipos de cadastros são obrigatórios.
    Funcionalidades de negócio: consiste em funcionalidades que aplicam operações ou regras de negócio específicas no sistema. Por exemplo, a funcionalidade “controle de estoque” aplicam operações e regras de negócio para fazer esse controle. Pelo menos um tipo de regra de negócio é obrigatório.
    Autenticação e controle de usuário: todos os usuários administrativos devem realizar um login para acessar o sistema. Inicialmente, o sistema deve conter apenas um usuário admin. Esse usuário poderá cadastrar novos usuários, informando o CPF, nome, cargo na empresa, e-mail e username. A senha pode estar inicialmente no cadastro do novo usuário, mas o ideal é enviar por e-mail para o novo usuário com um link para este usuário cadastrar a senha. Os usuários cadastrados poderão editar seu perfil.

Avaliação

O conceito desse trabalho será baseado de acordo com as funcionalidades da aplicação e com os conceitos dos frameworks empregados no trabalho. Abaixo segue a relação dos requisitos a serem realizados no trabalho:

Conceito C:

    Aplicação inicial, realizando pelo menos dois cadastros;
    Realização apenas do login – contemplar apenas o usuário admin.
    Persistência de dados;
    Utilização adequada do Framework Back-end.

Conceito B:

    Realização das funcionalidades para o conceito C;
    Aplicação praticamente funcional - realizando os cadastros e a funcionalidade de negócio;
    Cadastros simples de usuários, edição de perfil e realização do login;
    Interface gráfica adequada;
    Utilização de um sistema de controle de versão (ex: git) e de um ambiente de colaboração e gerenciamento de código baseado nesse controle de versão (ex: github, bitbucket).

 Conceito A:

    Realização das funcionalidades para o conceito B;
    Aplicação totalmente funcional, com o controle de usuário (e login) realizado completamente, incluindo enviar e-mail para cadastrar/recadastrar senha;
    Utilização de alguma funcionalidade diferenciada: testes unitários, técnicas de caching, técnicas de segurança ou outras (ver com o professor);
    Implantação do sistema na nuvem ou em um serviço de hospedagem.

Apresentação e Entrega:

O trabalho deverá ser realizado individualmente. O aluno deverá estar presente em aula para apresentar o trabalho. A entrega deverá ser realizada pelo Blackboard (http://senac.blackboard.com), anexando o projeto como arquivo zipado (zip), contendo o código fonte e todas as bibliotecas extras utilizadas (alinhadas conforme o projeto). Caso o aluno tenha utilizado o Github e/ou implantado o sistema, deverá também relacionar o(s) link(s) como comentário.

Data de Entrega/Apresentação: 30/09



### Sites Visitados:

- https://www.dicascodeigniter.com.br/mini-crud-com-codeigniter-3-e-bootstrap/
- https://www.ibm.com/developerworks/br/opensource/library/os-codeigniter/index.html
- https://icon-icons.com/pt/icone/Fechar-excluir-remover/81512
- https://www.youtube.com/user/JLDRPT/videos
- https://laragon.org/
- https://learncodeweb.com/php/php-crud-in-bootstrap-4-with-search-functionality/
- https://bootsnipp.com/snippets/0BDPG
- https://www.grocerycrud.com/




